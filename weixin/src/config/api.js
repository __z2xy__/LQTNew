/**
 * Created by lume on 2017/10/18.
 *
 * 定义常量  - 网络接口请求
 *
 */

const apiRouter = {
  NOTICE_TYPE: roterPath + "/app/notice/type",
  SCOPE_DEPT: roterPath + "/app/notice/scope",
  SCOPE_SPACE: roterPath + "/app/notice/scope",
  SCOPE_OPTIONAL: roterPath + "/app/notice/scope",
  SCOPE_CURRICULUM: roterPath + "/app/notice/scope",
  SCOPE_GRADE: roterPath + "/app/notice/scope",

  // 查看通知
  LOOK_INFO: roterPath + "/app/notice/publist",
  DEL_INFO: roterPath + "/app/notice/del",
  EDLIT_INFO: roterPath + "/app/notice/info",

  // 首页
  GET_LIST: roterPath + "/app/profile/info",

  // 发布通知接口
  POST_SEND: roterPath + "/app/notice/publish",

  // 获取微信签名
  GET_CONFIG: roterPath + "/app/config/wechat?url=",


  // 评价接口

  GET_EVALUATE: roterPath + "/app/zh/teacher/scope",

  GET_CLASSBYCODE: roterPath + "/app/zh/teacher/classbycode?",

  GET_STUDENT: roterPath + "/app/zh/teacher/student?",
  GET_STUDENTINFO: roterPath + "/app/zh/teacher/studentinfo?",
  POST_EVALUATE: roterPath + "/app/zh/teacher/savescore",


  // 发布作业
  GET_HOMEWORKLIST: roterPath + "/app/homework/homeworkList",
  DEL_HOMEWORK: roterPath + "/app/homework/del",
  GET_HOMEWORKSCOPE: roterPath + "/app/homework/scope",
  POST_HOMEWORKPUBLISH: roterPath + "/app/homework/publish",

  // 图片视频上传
  POST_IMG: roterPath + "/app/upload/img",
  GET_VIDEO: roterPath + "/app/upload/video",

  SPACE_PHOTO: roterPath + "/app/space/publish",           // 图片 发布帖子
  SPACE_DETAIL : roterPath + "/app/space/topicinfo" ,     //  帖子详情
  SPACE_DEL: roterPath + "/app/space/topicdel"  ,         // 删除帖子
  BACK_SPACE: roterPath + '/weixin/wechat/views/class/home.jsp?SPACE_ID=',   // 返回空间






  // 工资单
  GET_WAGELIST: roterPath + "/app/wage/myWageList",
  GET_WAGEINFO: roterPath + "/app/wage/myWageData",

  //获取用户日历分类类型
  GET_CALENDAR_TYPE: roterPath + "/app/calendar/type",
  GET_CALENDAR_DEL: roterPath + '/app/calendar/delEvents',
  POST_CALENDAR_SAVE: roterPath + "/app/calendar/addEvents",
  GET_CALENDAR_DETAIL: roterPath + "/app/calendar/eventInfo",
  GET_CALENDAR_ALL_TYPE_TOTAL: roterPath + "/app/calendar/allTypeTotal",
  GET_CALENDAR_DATA_BY_TYPE: roterPath + "/app/calendar/dataByType",
  GET_CALENDAR_DATA_LIST: roterPath + "/app/calendar/dataList",
  GET_CALENDAR_KBWEEK: roterPath + '/app/calendar/kbWeek',
  GET_CALENDAR_KBWEEKDETAIL: roterPath +'/app/calendar/kbWeekDetail',
  GET_CALENDAR_KBWEEKBYDATE: roterPath +'/app/calendar/kbWeekByDate',
  GET_CALENDAR_WEEKSTART: roterPath +'/app/calendar/weekStart', //第一天为周日还是周一


  //问卷
  GET_QUESTION_SCOPE: roterPath + '/app/examen/scope',
  GET_QUESTION_SCOPEINFO: roterPath + '/app/examen/scopeInfo',
  POST_QUESTION_PUBLISH100: roterPath + '/app/examen/publish100',
  POST_QUESTION_PUBLISH200: roterPath + '/app/examen/publish200',
  POST_QUESTION_PUBLISH300: roterPath + '/app/examen/publish300',
  POST_QUESTION_PUBLISH400: roterPath + '/app/examen/publish400',

  GET_QUESTION_LIST_UP:roterPath+'/app/examen/managerList',//发布者问卷列表
  GET_QUESTION_LIST:roterPath+'/app/examen/infoList',//填写者问卷列表
  GET_QUESTION_TOTAL_INFO:roterPath+'/app/examen/totalInfo',
  GET_QUESTION_DETAIL:roterPath+'/app/examen/detail',//
  POST_QUESTION_SUBMIT:roterPath+'/app/examen/submit',
  GET_QUESTION_DELETE:roterPath+'/app/examen/del',

  GET_HOMEWORK_COUNT:roterPath+'/wechat/homeworkH5/getHomeWorkCount',//查询作业总数
  GET_HOMEWORK_TOP10:roterPath+'/wechat/homeworkH5/getHomeWorkTop10',//查询作业总数<10的列表接口
  GET_HOMEWORK_LIST:roterPath+'/wechat/homeworkH5/getHomeWorkListTest',//查询登录用户指定页的作业列表

  GET_HOMEWORK_STU_VIEW:roterPath+'/wechat/homeworkH5/homeworkStuView',//查询答题题目（做作业）
  GET_HOMEWORK_STU_ANSWER:roterPath+'/wechat/homeworkH5/homeworkStuAnswer',//会返回题目和答案的(看答案)
  GET_HOMEWORK_SAVE_ANSWER:roterPath+'/wechat/homeworkH5/saveAnswer',     //提交作业
  GET_HOMEWORK_BACK_COMMENT:roterPath+'/wechat/homeworkH5/getHomeWorkBackComment',     //提交作业
  GET_HOMEWORK_COMMENT_LIST:roterPath+'/wechat/homeworkH5/getCommentList',     //返回评语





}
export default apiRouter;






