import Vue from 'vue'
import Router from 'vue-router'

import Login from '../page/home/Login.vue'
import Home from '../page/home/ Home.vue'
import Type from '../page/home/Type.vue'
import Notice from '../page/home/Notice.vue'
import Grade from '../page/home/Grade.vue'
import Academic from '../page/home/Academic.vue'
import Curriculum from '../page/home/Curriculum.vue'
import Optional from '../page/home/Optional.vue'
import Space from '../page/home/Space.vue'
import Select from '../page/home/Select.vue'
import Posted from '../page/home/Posted.vue'
import Message from '../page/message/Message.vue'
import Evaluate from '../page/evaluate/EvaluateType.vue'
import GradeList from "../page/evaluate/GradeList.vue"
import RemarkInfo from "../page/evaluate/RemarkInfo.vue"
import Storage from "../page/evaluate/Storage.vue"

import JobList from "../page/publish/jobList.vue"
import Announce from "../page/publish/announce.vue"
import Publish from "../page/publish/publish.vue"

import PayrollList from "../page/payroll/PayrollList.vue"
import PayrollDetail from "../page/payroll/PayrollDetail.vue"

import Schedule from "../page/schedule/Schedule.vue"
import ScheduleAdd from "../page/schedule/ScheduleAdd.vue"
import ScheduleType from "../page/schedule/Type.vue"
import ScheduleWeek from "../page/schedule/Week.vue"
import ScheduleDetail from "../page/schedule/Detail.vue"



import spaceDetail from "../page/space/spaceDetail.vue"
import Photo from "../page/space/photo.vue"
import Video from "../page/space/video.vue"

import QuestionFirst from "../page/question/First.vue"
import QuestionFirstMod from "../page/question/FirstMod.vue"

import QuestionList from "../page/question/List.vue"

import QuestionNotice from "../page/question/Notice.vue"
import QuestionGrade from "../page/question/Grade.vue"
import QuestionCurriculum from "../page/question/Curriculum.vue"
import QuestionOptional from "../page/question/Optional.vue"
import QuestionSpace from "../page/question/Space.vue"
import QuestionPosted from "../page/question/Posted.vue"

import AnsRadio from "../page/question/detail/Radio.vue"
import AnsCheck from "../page/question/detail/CheckBox.vue"
import AnsEnroll from "../page/question/detail/Enroll.vue"
import AnsScoring from "../page/question/detail/Scoring.vue"

import CountAnsRadio from "../page/question/count-detail/Radio.vue"
import CountAnsCheck from "../page/question/count-detail/CheckBox.vue"
import CountAnsEnroll from "../page/question/count-detail/Enroll.vue"
import CountAnsScoring from "../page/question/count-detail/Scoring.vue"

import questionResult from "../page/question/Result.vue"
import questionResult2 from "../page/question/Result2.vue"
import questionResult3 from "../page/question/Result3.vue"


import QuestionListUp from "../page/question/ListUp.vue"
import ListUser from "../page/question/ListUser.vue"


import WorkListUser from "../page/work/ListUser.vue"
import WorkDetail from "../page/work/Detail.vue"
import WorkDetailDone from "../page/work/DetailDone.vue"
import WorkUserRevised from "../page/work/Revised.vue"





Vue.use(Router)
export default new Router({
 // mode: 'history',
  routes: [
    // 发布通知
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/type',
      name: 'type',
      component: Type
    },
    {
      path: '/notice',
      name: 'notice',
      component: Notice
    },
    {
      path: '/grade',
      name: 'grade',
      component: Grade
    },
    {
      path: '/academic',
      name: 'academic',
      component: Academic
    },
    {
      path: '/curriculum',
      name: 'curriculum',
      component: Curriculum
    },
    {
      path: '/optional',
      name: 'optional',
      component: Optional
    },
    {
      path: '/space',
      name: 'space',
      component: Space
    },
    {
      path: '/select',
      name: 'select',
      component: Select
    },
    {
      path: '/posted',
      name: 'posted',
      component: Posted
    },


    // 通知列表
    {
      path: '/message',
      name: 'message',
      component: Message
    },


    // 评价
    {
      path: '/evaluate',
      name: 'evaluate',
      component: Evaluate
    },

    {
      path: '/gradeList',
      name: 'gradeList',
      component: GradeList
    },

    {
      path: '/remarkInfo',
      name: 'remarkInfo',
      component: RemarkInfo
    },

    {
      path: '/storage',
      name: 'storage',
      component: Storage
    },


    {
      path: '/jobList',
      name: 'jobList',
      component: JobList
    },

    {
      path: '/announce',
      name: 'announce',
      component: Announce
    },
    {
      path: '/publish',
      name: 'publish',
      component: Publish
    },

    // 工资单
    {
      path: '/payrolllist',
      name: 'payrolllist',
      component: PayrollList
    },
    {
      path: '/payrolldetail',
      name: 'payrolldetail',
      component: PayrollDetail
    },
    //课程表
    {
      path: '/schedule',
      name: 'schedule',
      component: Schedule
    },
    {
      path: '/schedule-add',
      name: 'ScheduleAdd',
      component: ScheduleAdd
    },
    {
      path: '/schedule-type',
      name: 'ScheduleType',
      component: ScheduleType
    },
    {
      path: '/schedule-week',
      name: 'ScheduleWeek',
      component: ScheduleWeek
    },
    {
      path: '/schedule-detail',
      name: 'ScheduleDetail',
      component: ScheduleDetail
    },



    {
      path: '/spaceDetail',
      name: 'spaceDetail',
      component: spaceDetail
    },

    //  发布图片
    {
      path: '/photo',
      name: 'photo',
      component: Photo
    },

    // 发布视频
    {
      path: '/video',
      name: 'video',
      component: Video
    },

    //发起问卷
    {
      path: '/q-type',
      name: 'questionFirst',
      component: QuestionFirst
    },
    {
      path: '/q-scope',
      name: 'questionList',
      component: QuestionList
    },

    {
      path: '/question-notice',
      name: 'questionNotice',
      component: QuestionNotice
    },
    {
      path: '/question-grade',
      name: 'questionGrade',
      component: QuestionGrade
    },
    {
      path: '/question-curriculum',
      name: 'questionCurriculum',
      component: QuestionCurriculum
    },
    {
      path: '/question-optional',
      name: 'questionOptional',
      component: QuestionOptional
    },
    {
      path: '/question-space',
      name: 'questionSpace',
      component: QuestionSpace
    },
    {
      path: '/question-posted',
      name: 'questionPosted',
      component: QuestionPosted
    },
    {
      path: '/ans-radio',
      name: 'ansRadio',
      component: AnsRadio
    },
    {
      path: '/ans-check',
      name: 'ansCheck',
      component: AnsCheck
    },
    {
      path: '/ans-enroll',
      name: 'ansEnroll',
      component: AnsEnroll
    },
    {
      path: '/ans-scoring',
      name: 'ansScoring',
      component: AnsScoring
    },


    {
      path: '/ans-radioc',
      name: 'countAnsRadio',
      component: CountAnsRadio
    },
    {
      path: '/ans-checkc',
      name: 'ansCheckc',
      component: CountAnsCheck
    },
    {
      path: '/ans-enrollc',
      name: 'ansEnrollc',
      component: CountAnsEnroll
    },
    {
      path: '/ans-scoringc',
      name: 'ansScoringc',
      component: CountAnsScoring
    },
    {
      path: '/question-result',
      name: 'questionResult',
      component: questionResult
    },
    {
      path: '/question-result2',
      name: 'questionDetail2',
      component: questionResult2
    },{
      path: '/question-detail3',
      name: 'questionDetail3',
      component: questionResult3
    },
    {
      path: '/qa_list_up',
      name: 'questionListUp',
      component: QuestionListUp
    },
    {
      path: '/qa_list_user',
      name: 'listUser',
      component: ListUser
    },
    {
      path: '/q-type-m',
      name: 'questionFirstMod',
      component: QuestionFirstMod
    },

    {
      path: '/work-list-user',
      name: 'workListUser',
      component: WorkListUser
    },
    {
      path: '/work-detail',
      name: 'workDetail',
      component: WorkDetail
    },
    {
      path: '/work-detail-done',
      name: 'workDetailDone',
      component: WorkDetailDone
    },
    {
      path: '/work-user-rev',
      name: 'workUserRevised',
      component: WorkUserRevised
    },



    {
      path: '/login',
      name: 'login',
      component: Login
    },


  ]
})
